//
//  mapper.swift
//  Form
//
//  Created by MSU IT CS on 31/1/2562 BE.
//  Copyright © 2562 MSU IT CS. All rights reserved.
//

import ObjectMapper


class ModelUserInfo:Mappable{
    
    var username:String?
    var name:String?
    var email:String?
    var position:String?
    var tel:String?
    
    required init?(map: Map) {
        
    }
    
   func mapping(map: Map) {
        username <- map["username"]
        name <- map["name"]
        email <- map["email"]
        position <- map["position"]
        tel <- map["tel"]
    }
    
    
    
}
