//
//  CheckLogout.swift
//  Form
//
//  Created by MSU IT CS on 1/2/2562 BE.
//  Copyright © 2562 MSU IT CS. All rights reserved.
//

import Foundation
import Alamofire
import ObjectMapper

class CheckLogout:Mappable{
    
    var message:String?
    required init?(map: Map) {
        
    }
    
     func mapping(map: Map) {
        message <- map["message"]
    }
    
    typealias Response = (CheckLogout?,Error?) -> ()
    
    class func check(_ url:URL,token:String,completion: @escaping Response ){
        
        let headers: HTTPHeaders = [
            "token": token,
            //  "Content-Type": "application/json"
        ]
        Alamofire.request(url,method: .delete,parameters: nil, headers:nil).validate().responseJSON { response in
            
            switch response.result {
            case .success(let json):
                
                let array = Mapper<CheckLogout>().map(JSONObject: json as AnyObject)
                //mapArray(JSONObject: json as AnyObject)!
                
                completion(array,nil)
                
            case .failure(let error):
                completion(nil,error)
        }
    }
    
}
}
