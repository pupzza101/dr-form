//
//  CheckLogin.swift
//  Form
//
//  Created by MSU IT CS on 30/1/2562 BE.
//  Copyright © 2562 MSU IT CS. All rights reserved.
//

import Foundation
import Alamofire
import ObjectMapper



class checkLogin : Mappable {
    
    var token:String?
    var userinfo: ModelUserInfo?
    
    required init?(map: Map) {
        
    }
    
     func mapping(map: Map) {
        token <- map["token"]
        userinfo <- map["user_info"]
        
    }
    
    typealias WebServiceResponse = ([checkLogin]?,Error?) -> ()
    typealias Response = (checkLogin?,Error?) -> ()
    
    class func check(_ url:URL,username:String,password:String,completion: @escaping Response)
    {
        var params = [String:AnyObject]()
        params["username"] = username as AnyObject
        params["password"] = password as AnyObject
        
        var user = [String:AnyObject]()
        user["session"] = params as AnyObject
        
        Alamofire.request(url, method: .post, parameters: user, headers: nil).validate().responseJSON { response in
            switch response.result {
            case .success(let json):
                
                let array = Mapper<checkLogin>().map(JSONObject: json as AnyObject)
                //mapArray(JSONObject: json as AnyObject)!
                
                completion(array,nil)
                
            case .failure(let error):
                completion(nil,error)
            }
        }
    }
}
