//
//  LogInView.swift
//  Form
//
//  Created by MSU IT CS on 30/1/2562 BE.
//  Copyright © 2562 MSU IT CS. All rights reserved.
//

import UIKit
import Alamofire

extension UIViewController {
    
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}

let url = URL(string:"http://192.168.1.152:3000")

class LogInView: UIViewController {

    var token:String? = nil
    var username:String?
    var name:String?
    var email:String?
    var position:String?
    var tel:String?
    
    var mapper:ModelUserInfo?
    var info:ModelUserInfo?
    override func viewDidLoad() {
        super.viewDidLoad()
       self.hideKeyboardWhenTappedAround()
        // Do any additional setup after loading the view.
    }
  
    @IBOutlet weak var UsernameText: UITextField!
    @IBOutlet weak var PasswordText: UITextField!
    
    
    @IBAction func SigninBtn(_ sender: Any) {
        
        let username:String? = UsernameText.text
        let password:String? = PasswordText.text
        let link = URL(string:"\(url!)/login")
        checkLogin.check(link!, username: username!, password: password!){(result,error) in
            self.token = result?.token
            self.info = result?.userinfo
         // self.mapper = info
            
            if self.token != nil{
                
                 self.performSegue(withIdentifier: "FormView", sender: self)
            }else{
                let alertController = UIAlertController(title: "Username/Password incorrect", message: "", preferredStyle: .alert)
                let action1 = UIAlertAction(title: "Ok", style: .default) { (action:UIAlertAction) in
                   
                }
                alertController.addAction(action1)
                self.present(alertController, animated: true, completion: nil)
                
                
            }
        }
     //
    }
  
   
    @IBAction func SignupBtn(_ sender: Any) {
         self.performSegue(withIdentifier: "SingUpView", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "FormView"{
            let next = segue.destination as! ContenerView
            next.token = self.token
            next.username = self.info?.username
            next.name = self.info?.name
            next.email = self.info?.email
            next.position = self.info?.position
            next.tel = self.info?.tel
            
        }
        
    }
    
    
}
