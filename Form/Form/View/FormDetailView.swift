//
//  FormDetailView.swift
//  Form
//
//  Created by MSU IT CS on 5/2/2562 BE.
//  Copyright © 2562 MSU IT CS. All rights reserved.
//

import UIKit

class FormDetailView: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let createButton = UIBarButtonItem(title: "Save", style: .done, target: self, action: #selector(createRecord))
        
        self.navigationItem.rightBarButtonItem = createButton
        
        // Do any additional setup after loading the view.
    }
    
    @objc func createRecord(){
        print("tap tap")
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
