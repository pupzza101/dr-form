//
//  SignUpView.swift
//  Form
//
//  Created by MSU IT CS on 31/1/2562 BE.
//  Copyright © 2562 MSU IT CS. All rights reserved.
//

import UIKit

class SignUpView: UIViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate {

    @IBOutlet weak var usernameText: UITextField!
    @IBOutlet weak var passwordText: UITextField!
    @IBOutlet weak var emailText: UITextField!
    @IBOutlet weak var nameText: UITextField!
    @IBOutlet weak var TelText: UITextField!
    @IBOutlet weak var positionText: UITextField!
    @IBOutlet weak var imageView: UIImageView!
    
    var base64String:String?
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func importimageBtn(_ sender: Any) {
        let image = UIImagePickerController()
        image.delegate = self
        
        let actionSheet = UIAlertController(title: "Photo Source",message: "Choose a source ", preferredStyle: .actionSheet)
        
        actionSheet.addAction(UIAlertAction(title: "Camera", style: .default, handler: { (action:UIAlertAction) in
            
            
            
            image.sourceType = .camera
           // image.mediaTypes = kUTTypeImage as String
            image.allowsEditing = false
            
            self.present(image ,animated: true,completion: nil)
        }))
        actionSheet.addAction(UIAlertAction(title: "Photo Libraby", style: .default, handler: { (action:UIAlertAction) in
            
            image.sourceType = UIImagePickerController.SourceType.photoLibrary
            
            image.allowsEditing = false
            self.present(image ,animated: true)
            
            
            
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil ))
        
        self.present(actionSheet , animated: true ,completion: nil)
        
    }
    
    
    @IBAction func createBtn(_ sender: Any) {
        
        let username = self.usernameText.text
        let password = self.passwordText.text
        let email = self.emailText.text
        let name = self.nameText.text
        let tel = self.TelText.text
        let position = self.positionText.text
        var mapper: ModelUserInfo?
        
        
        let link = URL(string:"\(url!)/users")

        if usernameText.text?.isEmpty ?? true || emailText.text?.isEmpty ?? true || nameText.text?.isEmpty ?? true || TelText.text?.isEmpty ?? true || positionText.text?.isEmpty ?? true || passwordText.text?.isEmpty ?? true
        {
            print("error")
//
//
        }else{
            Signup.create(link!, username: username!, password: password!, name: name!, email: email!, tel: tel!, position: position!,image: base64String!) {(result,error) in
                  var x = result?.message
                     print(result?.message)
                    if x == "Sign up Complete" {
                     let alertController = UIAlertController(title: "Sign up Complete", message: "", preferredStyle: .alert)
                            let action1 = UIAlertAction(title: "Ok", style: .default) { (action:UIAlertAction) in
                                self.performSegue(withIdentifier: "loginview", sender: self)
                            }
                            alertController.addAction(action1)
                            self.present(alertController, animated: true, completion: nil)
                        }else{
                                print(result?.message)
                
                
                            }
                         }
        }
    }
    
    
    @IBAction func backBtn(_ sender: Any) {
        
        self.performSegue(withIdentifier: "loginview", sender: self)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
        {
            
            
            
            base64String = convertImageTobase64(format: .jpeg(1.0), image: image)!
            
            
            // print("++++++++++\(base64String)")
            imageView.image = image
            
            
        }
        
        self.dismiss(animated: true, completion: nil)
        
        
    }
    public enum ImageFormat {
        case png
        case jpeg(CGFloat)
    }
    func convertImageTobase64(format: ImageFormat, image:UIImage) -> String? {
        var imageData: Data?
        switch format {
        case .png: imageData = image.pngData()
        case .jpeg(let compression): imageData = image.jpegData(compressionQuality: compression)
        }
        return imageData?.base64EncodedString()
    }
    
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
