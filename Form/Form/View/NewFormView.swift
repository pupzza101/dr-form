//
//  NewFormView.swift
//  Form
//
//  Created by MSU IT CS on 30/1/2562 BE.
//  Copyright © 2562 MSU IT CS. All rights reserved.
//

import UIKit
import Photos
import AVKit
import AVFoundation
import MobileCoreServices

struct  CellData {
    let message:String?
}
class NewFormView: UIViewController,UINavigationControllerDelegate,UIImagePickerControllerDelegate,UITableViewDelegate,UITableViewDataSource {
   
    
    @IBOutlet weak var tabelview: UITableView!
    
    
    @IBOutlet weak var titleText: UITextField!
    @IBOutlet weak var DescriptionText: UITextView!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var imageBtn: UIButton!
    
    var token:String?
    var username:String?
    var name:String?
    var email:String?
    var position:String?
    var tel:String?
    var x = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("New Form: \(token)")
         self.hideKeyboardWhenTappedAround()
        let createButton = UIBarButtonItem(title: "Save", style: .done, target: self, action: #selector(createFormBtn))
       
        self.navigationItem.rightBarButtonItem = createButton
        // Do any additional setup after loading the view.
        
    }
    @objc func createFormBtn(){
        print("tap tap")
    }
    @IBAction func importImage(_ sender: Any) {
        
        let image = UIImagePickerController()
        image.delegate = self
        
        let actionSheet = UIAlertController(title: "Photo Source",message: "Choose a source ", preferredStyle: .actionSheet)
        
        actionSheet.addAction(UIAlertAction(title: "Camera", style: .default, handler: { (action:UIAlertAction) in
            
            
            
            image.sourceType = .camera
            image.mediaTypes = [kUTTypeImage as String]
            image.allowsEditing = false
           
            self.present(image ,animated: true,completion: nil)
        }))
        actionSheet.addAction(UIAlertAction(title: "Photo Libraby", style: .default, handler: { (action:UIAlertAction) in
            
             image.sourceType = UIImagePickerController.SourceType.photoLibrary
           
             image.allowsEditing = false
             self.present(image ,animated: true)
            
          
            
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil ))
        
        self.present(actionSheet , animated: true ,completion: nil)
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
        {
            
            
            // print("++++++++++\(base64String)")
            imageView.image = image
            
            
        }
        
        
        self.dismiss(animated: true, completion: nil)
        
        
    }
    
   
    @IBAction func addColum(_ sender: Any) {
        x = x+1
        self.tabelview.reloadData()
    }
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return x
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! NewFormCell
   //     cell.delegate = self as! alert
         
        return cell
    }
}
