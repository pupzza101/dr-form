//
//  FormView.swift
//  Form
//
//  Created by MSU IT CS on 30/1/2562 BE.
//  Copyright © 2562 MSU IT CS. All rights reserved.
//

import UIKit

class ContenerView: UIViewController {

    var token:String?
    var username:String?
    var name:String?
    var email:String?
    var position:String?
    var tel:String?
    
    @IBOutlet weak var sideMenuConstraint: NSLayoutConstraint!
    var sideMenuOpen = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        NotificationCenter.default.addObserver( self, selector: #selector(toggleSideMenu), name: NSNotification.Name("ToggleSideMenu"), object: nil)
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "mainformsegue"{
            let next = segue.destination.children[0] as! MainFormView
            next.token = self.token
            next.username = self.username
            next.name = self.name
            next.email = self.email	
            next.position = self.position
            next.tel = self.tel
        }
        
    }
    
    @objc func toggleSideMenu(){
        if sideMenuOpen {
            sideMenuOpen = false
            sideMenuConstraint.constant = -240
        }else{
            sideMenuOpen = true
            sideMenuConstraint.constant = 0
        }
        UIView.animate(withDuration: 0.3)
        {
            self.view.layoutIfNeeded()
        }
    }

    
}
