//
//  ProfileView.swift
//  Form
//
//  Created by MSU IT CS on 1/2/2562 BE.
//  Copyright © 2562 MSU IT CS. All rights reserved.
//

import UIKit

class ProfileView: UIViewController {
    var token:String?
    var username:String?
    var name:String?
    var email:String?
    var position:String?
    var tel:String?
    
    @IBOutlet weak var usernametext: UITextField!
    @IBOutlet weak var positiontext: UITextField!
    @IBOutlet weak var teltext: UITextField!
    @IBOutlet weak var nametext: UITextField!
    @IBOutlet weak var emailtext: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        usernametext.text = username
        emailtext.text = email
        nametext.text = name
        teltext.text = tel
        positiontext.text = position
        // Do any additional setup after loading the view.
    }
    
    @IBAction func logoutBtn(_ sender: Any) {
        
        let link = URL(string:"\(url!)/logout")
        CheckLogout.check(link!, token: token!){
            (result,error) in
            print(result?.message)
          
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
