//
//  MainFormView.swift
//  Form
//
//  Created by MSU IT CS on 30/1/2562 BE.
//  Copyright © 2562 MSU IT CS. All rights reserved.
//

import UIKit

class MainFormView: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource {
    
    var myform = ["1","2","3","4","5","6"]
    var sharewithme = ["4","5","6","6","6","6","6","6","6","6","6","6","6","6","6","6"]
    
    var token:String?
    var username:String?
    var name:String?
    var email:String?
    var position:String?
    var tel:String?
    
    @IBOutlet weak var myFormCollectionView: UICollectionView!
    @IBOutlet weak var shareWithMeCollectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
 //       print("Mainform Token : \(token!)")
        myFormCollectionView.delegate = self
        myFormCollectionView.dataSource = self
        
        shareWithMeCollectionView.delegate = self
        shareWithMeCollectionView.dataSource = self
        
        self.view.addSubview(myFormCollectionView)
        self.view.addSubview(shareWithMeCollectionView)
        
        // Do any additional setup after loading the view.
        
        NotificationCenter.default.addObserver( self, selector: #selector(showprofile), name: NSNotification.Name("profilesegue"), object: nil)
    }
    
    @objc func showprofile(){
        performSegue(withIdentifier: "profilesegue", sender: nil)
    }
    
    @IBAction func createBtn(_ sender: Any) {
    self.performSegue(withIdentifier: "newformviewsegue", sender: self)
        
        
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "newformviewsegue"{
            let next = segue.destination as! NewFormView
            next.token = self.token
            next.username = self.username
            next.name = self.name
            next.email = self.email
            next.position = self.position
            next.tel = self.tel

        }else if segue.identifier == "profilesegue"{
            let next = segue.destination as! ProfileView
            next.token = self.token
            next.username = self.username
            next.name = self.name
            next.email = self.email
            next.position = self.position
            next.tel = self.tel
        }
        
        
    }
    
    
    @IBAction func SideMenu(){
    
        NotificationCenter.default.post(name: NSNotification.Name("ToggleSideMenu"),object:nil)
        
    }
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if collectionView == myFormCollectionView{
            return myform.count
        }
        else{
            return sharewithme.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == self.myFormCollectionView{
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "myFormCell", for:  indexPath) as! MyFormCollectionViewCell
            
            cell.titleLabel.text = myform[indexPath.row]
            
                return cell
            
            
        }else{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "shareWithMeCell", for:  indexPath) as! ShareWithMeCollectionViewCell
            cell.titleLabel.text = sharewithme[indexPath.row]
            
            return cell
        }
    }
    

  


}
