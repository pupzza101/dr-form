//
//  ViewController.swift
//  Form
//
//  Created by MSU IT CS on 28/1/2562 BE.
//  Copyright © 2562 MSU IT CS. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func viewDidAppear(_ animated: Bool) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.5) { // Change `2.0` to the desired number of seconds.
            // Code you want to be delayed
             self.performSegue(withIdentifier: "loginView", sender: self)
        }
       
    }
    
    
    
    
}

